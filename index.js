"use strict";

const EventEmitter = require('events');

class ChatApp extends EventEmitter {
    /**
     * @param {String} title
     */
    constructor(title) {
        super();

        this.title = title;

        // Посылать каждую секунду сообщение
        setInterval(() => {
            this.emit('message', `${this.title}: ping-pong`);
        }, 1000);
    }

    close() {
        this.emit('close', `Чат вконтакте закрылся :(`);
    };
}

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk').setMaxListeners(2);

let chatOnMessage = (message) => {
  console.log(message);
};

let chatOnReady = () => {
    console.log('Готовлюсь к ответу');
};

let chatOnClose = (close) => {
    console.log(close);
};

vkChat.on('close', chatOnClose);
vkChat.close();

webinarChat.on('message', chatOnMessage);
webinarChat.on('message', chatOnReady);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage).on('message', chatOnReady);


// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
vkChat.removeListener('message', chatOnMessage);
}, 10000 );


// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
facebookChat.removeListener('message', chatOnMessage);
}, 15000 );

// Закрыть webinarChat
setTimeout( ()=> {
    console.log('Закрываю webinarChat. Всем спасибо, все свободны!');
webinarChat.removeListener('message', chatOnMessage);
}, 30000 );